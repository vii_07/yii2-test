<?php

namespace app\modules\contact\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ContactsTags[] $contactsTags
 */
class Tags extends \yii\db\ActiveRecord
{
	const SEARCH_LIMIT = 5;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'tags';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['name'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getContactsTags()
	{
		return $this->hasMany(ContactsTags::className(), ['tag_id' => 'id']);
	}

	/**
	 * search tags by term
	 * @param string $term
	 */
	public static function searchByTerm($term, $tags)
	{
		$tag_names = explode(',', rtrim($tags, ','));
		$records = self::find()
			->where(['LIKE', 'name', $term])
			->andWhere(['NOT IN', 'name', $tag_names])
			->limit(self::SEARCH_LIMIT)
			->all();
		$tags = [];
		foreach ($records as $record) {
			$tags[] = [
				'id' => $record->id,
				'value' => $record->name,
			];
		}
		return $tags;
	}

	/**
	 * contact relation
	 * @return \yii\db\ActiveQuery
	 */
	public function getContacts()
	{
		return $this->hasMany(Contacts::className(), ['id' => 'contact_id'])
				->viaTable('{{contacts_tags}}', ['tag_id' => 'id']);
	}

	/**
	 * setup search function for filtering and sorting
	 * based on fullName field
	 */
	public function search($params)
	{
		$query = self::find();
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'attributes' => [
				'id',
				'name',
			]
		]);

		if (!($this->load($params))) {
			return $dataProvider;
		}
		if (!empty($this->name))
			$query->andWhere(['LIKE', 'name', $this->name]);

		return $dataProvider;
	}
}
