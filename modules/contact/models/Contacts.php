<?php

namespace app\modules\contact\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 *
 * @property ContactsTags[] $contactsTags
 */
class Contacts extends \yii\db\ActiveRecord
{
	public $tag_names;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'contacts';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['first_name', 'last_name', 'email', 'tag_names'], 'required'],
			[['first_name', 'last_name'], 'string', 'max' => 50],
			[['email'], 'string', 'max' => 150],
			[['email'], 'email'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getContactsTags()
	{
		return $this->hasMany(ContactsTags::className(), ['contact_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTags()
	{
		return $this->hasMany(Tags::className(), ['id' => 'tag_id'])
				->viaTable('{{contacts_tags}}', ['contact_id' => 'id']);
	}

	/**
	 * save contact tags and relations
	 */
	protected function saveContactTags()
	{
		$tag_names = explode(',', rtrim($this->tag_names, ','));
		$new_tags = Tags::find()
			->where(['IN', 'name', $tag_names])
			->all();
		$exist_tags = $this->getTagsNames();
		foreach ($new_tags as $tag) {
			if (!in_array($tag->name, $exist_tags))
				$this->link('tags', $tag);
		}
		$new_tags_names = $this->getTagsNames($new_tags);
		foreach ($this->tags as $exist_tag) {
			if (!in_array($exist_tag->name, $new_tags_names))
				$this->unlink('tags', $exist_tag, TRUE);
		}
	}

	/**
	 * get only tags names from array of objects
	 * @param type $tags
	 * @return array
	 */
	protected function getTagsNames($tags = FALSE)
	{
		$tags = empty($tags) ? $this->tags : $tags;
		$tag_names = [];
		foreach ($tags as $tag) {
			$tag_names[] = $tag->name;
		}
		return $tag_names;
	}

	/**
	 * actions after find from database
	 */
	public function afterFind()
	{
		$this->tag_names = implode(',', $this->getTagsNames());
		if (\Yii::$app->controller->action->id == 'update')
			$this->tag_names .= ',';
	}

	/**
	 * save contact and relations with tags with transaction
	 * @return boolean
	 */
	public function saveWithTags()
	{
		$connection = \Yii::$app->db;
		$transaction = $connection->beginTransaction();
		try {
			$isNewContact = $this->isNewRecord;
			$this->save();
			$this->saveContactTags();
			if ($isNewContact)
				$this->sendEmail();
			$transaction->commit();
			return true;
		} catch (Exception $e) {
			$transaction->rollback();
		}
		return false;
	}

	/**
	 * setup search function for filtering and sorting
	 * based on fullName field
	 */
	public function search($params)
	{
		$query = self::find();
		$dataProvider = new \yii\data\ActiveDataProvider([
			'query' => $query,
		]);

		$dataProvider->setSort([
			'attributes' => [
				'id',
				'first_name',
				'last_name',
				'email',
			]
		]);

		if (!($this->load($params))) {
			return $dataProvider;
		}
		if (!empty($this->first_name))
			$query->andWhere(['LIKE', 'first_name', $this->first_name]);
		if (!empty($this->last_name))
			$query->andWhere(['LIKE', 'last_name', $this->last_name]);
		if (!empty($this->email))
			$query->andWhere(['LIKE', 'email', $this->email]);

		return $dataProvider;
	}

	public function sendEmail()
	{
		$subject = 'New Coctact';
		$body = 'Name: ' . $this->first_name . ' ' . $this->last_name . '; ' .
			'Tags: ' . implode(', ', $this->getTagsNames());
		Yii::$app->mailer->compose()
			->setTo($this->email)
			->setFrom([\Yii::$app->params['adminEmail'] => $subject])
			->setSubject($subject)
			->setTextBody($body)
			->send();
	}
}
