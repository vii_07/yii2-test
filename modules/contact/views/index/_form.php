<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\contact\models\Tags

/* @var $this yii\web\View */
/* @var $model app\modules\contact\models\Contacts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contacts-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'tag_names')->textInput(['maxlength' => true, 'placeholder' => 'Type at least one letter for search']) ?>

    <div class="form-group pull-right">
		<?=
		Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
		?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
