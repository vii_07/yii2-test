<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_093441_create_tables_for_contacts_and_tags extends Migration
{

	public function safeUp()
	{
		//create table contacts
		$this->createTable('{{contacts}}',
			[
			'id' => Schema::TYPE_PK,
			'first_name' => Schema::TYPE_STRING . '(50) NOT NULL',
			'last_name' => Schema::TYPE_STRING . '(50) NOT NULL',
			'email' => Schema::TYPE_STRING . '(150) NOT NULL',
		]);
		$this->createIndex('name', '{{contacts}}', ['first_name', 'last_name']);
		//create table tags
		$this->createTable('{{tags}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
		]);
		$this->createIndex('name', '{{tags}}', 'name');
		//create table contacts_tags
		$this->createTable('{{contacts_tags}}',
			[
			'id' => Schema::TYPE_PK,
			'contact_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'tag_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		]);
		$this->addForeignKey('contacts_tags_ibfk_1', '{{contacts_tags}}', 'contact_id', '{{contacts}}', 'id');
		$this->addForeignKey('contacts_tags_ibfk_2', '{{contacts_tags}}', 'tag_id', '{{tags}}', 'id');
	}

	public function safeDown()
	{
		$this->dropTable('{{contacts_tags}}');
		$this->dropTable('{{tags}}');
		$this->dropTable('{{contacts}}');
	}
}
