$(function () {
	$("#contacts-tag_names")
			// don't navigate away from the field on tab when selecting an item
			.on("keydown", function (event) {
				if (event.keyCode === $.ui.keyCode.TAB &&
						$(this).autocomplete("instance").menu.active) {
					event.preventDefault();
				}
			})
			.autocomplete({
				source: function (request, response) {
					$.getJSON("/index.php/contact/tag/get-all-tags", {
						term: extractLast(request.term),
						tags: $("#contacts-tag_names").val(),
					}, response);
				},
				search: function (event, ui) {
					// custom minLength
					var term = extractLast(this.value);
					if (term.length < 1) {
						return false;
					}
				},
				focus: function (event, ui) {
					// prevent value inserted on focus
					return false;
				},
				select: function (event, ui) {
					var terms = split(this.value);
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push(ui.item.value);
					// add placeholder to get the comma at the end
					terms.push("");
					this.value = terms.join(",");
					return false;
				},
			});
});
function split(val) {
	return val.split(/,\s*/);
}
function extractLast(term) {
	return split(term).pop();
}